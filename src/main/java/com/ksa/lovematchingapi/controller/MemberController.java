package com.ksa.lovematchingapi.controller;

import com.ksa.lovematchingapi.model.member.MemberCreateRequest;
import com.ksa.lovematchingapi.model.member.MemberItem;
import com.ksa.lovematchingapi.model.member.MemberResponse;
import com.ksa.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }
}
