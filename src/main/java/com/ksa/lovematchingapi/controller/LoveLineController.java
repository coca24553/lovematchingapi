package com.ksa.lovematchingapi.controller;

import com.ksa.lovematchingapi.entity.Member;
import com.ksa.lovematchingapi.model.loveLine.LoveLineChangeRequest;
import com.ksa.lovematchingapi.model.loveLine.LoveLineCreateRequest;
import com.ksa.lovematchingapi.model.loveLine.LoveLineItem;
import com.ksa.lovematchingapi.model.loveLine.LoveLineResponse;
import com.ksa.lovematchingapi.service.LoveLineService;
import com.ksa.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request) {
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "ok";
    }

    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines() {
        return loveLineService.getLoveLines();
    }

    @GetMapping("/detail/{id}")
    public LoveLineResponse getLoveLine(@PathVariable long id) {
        return loveLineService.getLoveLine(id);
    }

    @PutMapping("/phone-number/love-line-id/{love-line-id}")
    public String putLoveLine(@PathVariable long id, @RequestBody LoveLineChangeRequest request) {
        loveLineService.putLoveLine(id, request);

        return "ok";
    }
}
