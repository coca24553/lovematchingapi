package com.ksa.lovematchingapi.model.loveLine;

import com.ksa.lovematchingapi.entity.LoveLine;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineChangeRequest {
    private String lovePhoneNumber;
}
