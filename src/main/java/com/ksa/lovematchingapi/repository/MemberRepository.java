package com.ksa.lovematchingapi.repository;

import com.ksa.lovematchingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
