package com.ksa.lovematchingapi.service;

import com.ksa.lovematchingapi.entity.LoveLine;
import com.ksa.lovematchingapi.entity.Member;
import com.ksa.lovematchingapi.model.loveLine.LoveLineChangeRequest;
import com.ksa.lovematchingapi.model.loveLine.LoveLineCreateRequest;
import com.ksa.lovematchingapi.model.loveLine.LoveLineItem;
import com.ksa.lovematchingapi.model.loveLine.LoveLineResponse;
import com.ksa.lovematchingapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;


    public void setLoveLine(Member member, LoveLineCreateRequest request) {
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLoveLines() {
        List<LoveLine> originList = loveLineRepository.findAll();
        List<LoveLineItem> result = new LinkedList<>();

        for (LoveLine loveLine : originList) {
            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName()); //Lazy 문제 (가끔 있는 문제)
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public LoveLineResponse getLoveLine(long id) {
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        LoveLineResponse response = new LoveLineResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setMemberPhoneNumber(originData.getMember().getPhoneNumber());
        response.setMemberGender(originData.getMember().getIsMan() ? "man" : "woman");
        response.setLovePhoneNumber(originData.getLovePhoneNumber());

        return response;
    }

    public void putLoveLine(long id, LoveLineChangeRequest request) {
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        originData.setLovePhoneNumber(request.getLovePhoneNumber());
        loveLineRepository.save(originData);
    }
}

