package com.ksa.lovematchingapi.service;

import com.ksa.lovematchingapi.entity.Member;
import com.ksa.lovematchingapi.model.member.MemberCreateRequest;
import com.ksa.lovematchingapi.model.member.MemberItem;
import com.ksa.lovematchingapi.model.member.MemberResponse;
import com.ksa.lovematchingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();

        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setIsMan(request.getIsMan());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originData = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originData) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setIsMan(member.getIsMan());

            result.add(addItem);
        }
        return result;
    }
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setIsMan(originData.getIsMan() ? "남자" : "여자");

        return response;
    }
}
